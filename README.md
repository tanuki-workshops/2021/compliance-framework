# compliance-framework

https://docs.gitlab.com/ee/user/project/settings/#compliance-frameworks


Group owners can create, edit, and delete compliance frameworks:
- Go to the group’s Settings > General.
- Expand the Compliance frameworks section.

> 🖐️ **you must be on the highest level of the Group**

Compliance frameworks created can then be assigned to any number of projects using:
- The project settings page inside the group or subgroups.
- In GitLab 14.2 and later, using the GraphQL API.

TODO: with Ultimate it exists an impact with the CI 
